package mdsp_parameters

import (
	"time"
)

const (
	INPUT1                  = "INPUT1"
	INPUT2                  = "INPUT2"
	INPUT3                  = "INPUT3"
	INPUT4                  = "INPUT4"
	INPUT5                  = "INPUT5"
	INPUT6                  = "INPUT6"
	INPUT7                  = "INPUT7"
	INPUT8                  = "INPUT8"
	ENERGY_ACTIVE_TARIF_1   = "ENERGY_ACTIVE_TARIF_1"
	ENERGY_ACTIVE_TARIF_2   = "ENERGY_ACTIVE_TARIF_2"
	ENERGY_ACTIVE_TARIF_SUM = "ENERGY_ACTIVE_TARIF_SUM"
	BATTARY_LAST_MONTH      = "BATTARY_LAST_MONTH"
	PHASE_VOLTAGE_1         = "PHASE_VOLTAGE_1"
	PHASE_VOLTAGE_2         = "PHASE_VOLTAGE_2"
	PHASE_VOLTAGE_3         = "PHASE_VOLTAGE_3"
	PHASE_CURRENT_1         = "PHASE_CURRENT_1"
	PHASE_CURRENT_2         = "PHASE_CURRENT_2"
	PHASE_CURRENT_3         = "PHASE_CURRENT_3"
	PHASE_POWER_1           = "PHASE_POWER_1"
	PHASE_POWER_2           = "PHASE_POWER_2"
	PHASE_POWER_3           = "PHASE_POWER_3"
	RELE_STAT               = "RELE_STAT"
	DATE                    = "DATE"
	VERSION                 = "VERSION"
)

type Parameter struct {
	Id           int       `json:"id"      db:"id"`
	Ident        string    `json:"-"       db:"ident"`
	Title        string    `json:"title"   db:"title"`
	Divider      int       `json:"divider" db:"divider"`
	Round        int       `json:"round"   db:"round"`
	Time_running time.Time `json:"-"       db:"-"`
}

func New() *Parameter {
	return &Parameter{}
}

var MapParameters = map[string]Parameter{

	ENERGY_ACTIVE_TARIF_1: Parameter{
		Title:   "Активная энергия. Тариф 1",
		Divider: 1000,
		Round:   3},

	ENERGY_ACTIVE_TARIF_2: Parameter{
		Title:   "Активная энергия. Тариф 2",
		Divider: 1000,
		Round:   3},

	ENERGY_ACTIVE_TARIF_SUM: Parameter{
		Title:   "Активная энергия. Сумма",
		Divider: 1000,
		Round:   3},

	PHASE_VOLTAGE_1: Parameter{
		Title:   "Напряжение Фаза A",
		Divider: 1000,
		Round:   3},

	PHASE_VOLTAGE_2: Parameter{
		Title:   "Напряжение Фаза B",
		Divider: 1000,
		Round:   3},

	PHASE_VOLTAGE_3: Parameter{
		Title:   "Напряжение Фаза C",
		Divider: 1000,
		Round:   3},

	PHASE_CURRENT_1: Parameter{
		Title:   "Ток Фаза A",
		Divider: 1000,
		Round:   3},

	PHASE_CURRENT_2: Parameter{
		Title:   "Ток Фаза B",
		Divider: 1000,
		Round:   3},

	PHASE_CURRENT_3: Parameter{
		Title:   "Ток Фаза C",
		Divider: 1000,
		Round:   3},

	PHASE_POWER_1: Parameter{
		Title:   "Мощность Фаза A",
		Divider: 1000,
		Round:   3},

	PHASE_POWER_2: Parameter{
		Title:   "Мощность Фаза B",
		Divider: 1000,
		Round:   3},

	PHASE_POWER_3: Parameter{
		Title:   "Мощность Фаза C",
		Divider: 1000,
		Round:   3},

	"ENERGY_REACTIVE_TARIF_1": Parameter{
		Title:   "Реактивная энергия. Тариф 1",
		Divider: 1000,
		Round:   3},

	"ENERGY_REACTIVE_TARIF_2": Parameter{
		Title:   "Реактивная энергия. Тариф 2",
		Divider: 1000,
		Round:   3},

	"ENERGY_REACTIVE_TARIF_SUM": Parameter{
		Title:   "Реактивная энергия. Сумма",
		Divider: 1000,
		Round:   3},

	"ENERGY_FULL_TARIF_1": Parameter{
		Title:   "Полная энергия. Тариф 1",
		Divider: 1000,
		Round:   3},

	"ENERGY_FULL_TARIF_2": Parameter{
		Title:   "Полная энергия. Тариф 2",
		Divider: 1000,
		Round:   3},

	"ENERGY_FULL_TARIF_SUM": Parameter{
		Title:   "Полная энергия. Сумма",
		Divider: 1000,
		Round:   3},

	INPUT1: Parameter{
		Title:   "Вход 1",
		Divider: 1,
		Round:   0},

	INPUT2: Parameter{
		Title:   "Вход 2",
		Divider: 1,
		Round:   0},

	INPUT3: Parameter{
		Title:   "Вход 3",
		Divider: 1,
		Round:   0},

	INPUT4: Parameter{
		Title:   "Вход 4",
		Divider: 1,
		Round:   0},

	INPUT5: Parameter{
		Title:   "Вход 5",
		Divider: 1,
		Round:   0},

	INPUT6: Parameter{
		Title:   "Вход 6",
		Divider: 1,
		Round:   0},

	INPUT7: Parameter{
		Title:   "Вход 7",
		Divider: 1,
		Round:   0},

	INPUT8: Parameter{
		Title:   "Вход 8",
		Divider: 1,
		Round:   0},

	RELE_STAT: Parameter{
		Title:   "Статус реле",
		Divider: 1,
		Round:   0},

	DATE: Parameter{
		Title:   "Дата",
		Divider: 1,
		Round:   0},

	VERSION: Parameter{
		Title:   "Версия",
		Divider: 1,
		Round:   0},

	BATTARY_LAST_MONTH: Parameter{
		Title:   "Осталось заряда на мес.",
		Divider: 1,
		Round:   0},
}
